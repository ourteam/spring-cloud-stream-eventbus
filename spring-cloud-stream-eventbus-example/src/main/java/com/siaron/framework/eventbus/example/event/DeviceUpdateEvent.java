package com.siaron.framework.eventbus.example.event;


import com.siaron.framework.eventbus.annotation.Event;
import com.siaron.framework.eventbus.model.AbstractEvent;

/**
 * @author xielongwang
 * @create 2021/7/19 10:53 上午
 * @email siaron.wang@gmail.com
 * @description
 */
@Event(eventKey = "device.update.event",binder = "local_rabbit")
public class DeviceUpdateEvent extends AbstractEvent {

    private String device;

    private long ts;

    private String updateBy;

    private DeviceOnlineStatus online;

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public DeviceOnlineStatus getOnline() {
        return online;
    }

    public void setOnline(DeviceOnlineStatus online) {
        this.online = online;
    }
}

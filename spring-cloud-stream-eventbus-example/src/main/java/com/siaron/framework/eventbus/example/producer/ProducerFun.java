package com.siaron.framework.eventbus.example.producer;

import com.siaron.framework.eventbus.annotation.EventPublish;
import com.siaron.framework.eventbus.example.event.DeviceCreateEvent;
import com.siaron.framework.eventbus.example.event.DeviceOnlineStatus;
import com.siaron.framework.eventbus.example.event.DeviceUpdateEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;
import java.util.UUID;
import java.util.function.Supplier;

/**
 * @author xielongwang
 * @create 2021/7/19 10:52 上午
 * @email siaron.wang@gmail.com
 * @description
 */
@Configuration
public class ProducerFun {

    @Bean
    @EventPublish(binder = "local_rabbit")
    public Supplier<DeviceCreateEvent> deviceCreateEventSource() {
        return () -> {
            DeviceCreateEvent deviceCreateEvent = new DeviceCreateEvent();
            deviceCreateEvent.setDevice(UUID.randomUUID().toString());
            deviceCreateEvent.setCreateBy(String.valueOf(new Random().nextInt(100)));
            deviceCreateEvent.setOnline(DeviceOnlineStatus.ONLINE);
            deviceCreateEvent.setTs(System.currentTimeMillis());
            return deviceCreateEvent;
        };
    }

    @Bean
    @EventPublish(binder = "local_rabbit")
    public Supplier<DeviceUpdateEvent> deviceUpdateEventSource() {
        return () -> {
            DeviceUpdateEvent deviceUpdateEvent = new DeviceUpdateEvent();
            deviceUpdateEvent.setDevice(UUID.randomUUID().toString());
            deviceUpdateEvent.setUpdateBy(String.valueOf(new Random().nextInt(100)));
            deviceUpdateEvent.setOnline(DeviceOnlineStatus.ONLINE);
            deviceUpdateEvent.setTs(System.currentTimeMillis());
            return deviceUpdateEvent;
        };
    }
}

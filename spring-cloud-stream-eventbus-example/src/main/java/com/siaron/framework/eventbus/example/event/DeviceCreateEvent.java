package com.siaron.framework.eventbus.example.event;


import com.siaron.framework.eventbus.annotation.Event;
import com.siaron.framework.eventbus.model.AbstractEvent;

/**
 * @author xielongwang
 * @create 2021/7/15 10:47 上午
 * @email siaron.wang@gmail.com
 * @description
 */
@Event(eventKey = "device.create.event",binder = "local_rabbit")
public class DeviceCreateEvent extends AbstractEvent {

    private String device;

    private long ts;

    private String createBy;

    private DeviceOnlineStatus online;

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public DeviceOnlineStatus getOnline() {
        return online;
    }

    public void setOnline(DeviceOnlineStatus online) {
        this.online = online;
    }
}

package com.siaron.framework.eventbus.example.event;

/**
 * @author xielongwang
 * @create 2021/7/19 10:57 上午
 * @email siaron.wang@gmail.com
 * @description
 */
public enum DeviceOnlineStatus {
    /**
     * 在线
     */
    ONLINE,

    /**
     * 离线
     */
    OFFLINE;
}

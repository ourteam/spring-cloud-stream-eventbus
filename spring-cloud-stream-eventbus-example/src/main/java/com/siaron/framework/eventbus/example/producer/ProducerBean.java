package com.siaron.framework.eventbus.example.producer;

import com.siaron.framework.eventbus.annotation.EventPublish;
import com.siaron.framework.eventbus.example.event.DeviceCreateEvent;
import org.springframework.stereotype.Component;

import java.util.function.Supplier;

/**
 * @author xielongwang
 * @create 2021/7/22 10:33 上午
 * @email siaron.wang@gmail.com
 * @description
 */
@Component("p1")
@EventPublish(binder = "local_rabbit")
public class ProducerBean implements Supplier<DeviceCreateEvent> {
    @Override
    public DeviceCreateEvent get() {
        DeviceCreateEvent deviceCreateEvent=new DeviceCreateEvent();
        deviceCreateEvent.setCreateBy("ProducerBean XXXXXX");
        return deviceCreateEvent;
    }
}

package com.siaron.framework.eventbus.example.consumer;

import com.siaron.framework.eventbus.annotation.EventSubscribe;
import com.siaron.framework.eventbus.example.event.DeviceCreateEvent;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

/**
 * @author xielongwang
 * @create 2021/7/21 8:09 下午
 * @email siaron.wang@gmail.com
 * @description
 */
@Component("c1")
@EventSubscribe(group = "q1", consumer = 11, binder = "local_rabbit")
public class ConsumerBean implements Consumer<DeviceCreateEvent> {

    @Override
    public void accept(DeviceCreateEvent deviceCreateEvent) {

        System.out.println("ConsumerBean xxxxx:"+deviceCreateEvent);
    }
}

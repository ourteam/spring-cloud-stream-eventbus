package com.siaron.framework.eventbus.example.consumer;

import com.siaron.framework.eventbus.annotation.EventSubscribe;
import com.siaron.framework.eventbus.example.event.DeviceCreateEvent;
import com.siaron.framework.eventbus.example.event.DeviceUpdateEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Consumer;

/**
 * @author xielongwang
 * @create 2021/7/15 10:46 上午
 * @email siaron.wang@gmail.com
 * @description
 */
@Configuration
public class ConsumerFun {

    @Bean
    @EventSubscribe(group = "device.create.event.queue.c1", consumer = 11, binder = "local_rabbit")
    public Consumer<DeviceCreateEvent> deviceCreateEventConsumer1() {
        return dce -> {
            System.out.println("deviceCreateEventConsumer1: " + dce.getCreateBy());
        };
    }

    @Bean
    @EventSubscribe(group = "device.update.event.queue.c1", consumer = 2, binder = "local_rabbit")
    public Consumer<DeviceUpdateEvent> deviceUpdateEventConsumer1() {
        return dce -> {
            System.out.println("deviceUpdateEventConsumer1 : " + dce.getUpdateBy());
        };
    }

    @Bean
    @EventSubscribe(group = "device.update.event.queue.c2", binder = "local_rabbit")
    public Consumer<DeviceUpdateEvent> deviceUpdateEventConsumer2() {
        return dce -> {
            System.out.println("deviceUpdateEventConsumer2 : " + dce.getUpdateBy());
        };
    }

    @Bean
    @EventSubscribe(group = "device.update.queue.event.cc", binder = "local_rabbit")
    public Consumer<DeviceUpdateEvent> deviceUpdateEventConsumer3() {
        return dce -> {
            System.out.println("deviceUpdateEventConsumer3 : " + dce.getUpdateBy());
        };
    }
}

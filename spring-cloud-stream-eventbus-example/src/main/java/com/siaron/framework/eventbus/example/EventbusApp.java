package com.siaron.framework.eventbus.example;

import com.siaron.framework.eventbus.annotation.EnableEventBus;
import com.siaron.framework.eventbus.example.event.DeviceCreateEvent;
import com.siaron.framework.eventbus.publisher.EventPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * @author xielongwang
 * @create 2021/7/14 7:38 下午
 * @email siaron.wang@gmail.com
 * @description 事件bus
 */
@EnableEventBus(basePackages = {
        "com.siaron.framework.eventbus.example.consumer",
        "com.siaron.framework.eventbus.example.event",
        "com.siaron.framework.eventbus.example.producer"})
@SpringBootApplication
public class EventbusApp implements CommandLineRunner {

    @Autowired
    public EventPublisher eventPublisher;

    public static void main(String[] args) {
        new SpringApplicationBuilder(EventbusApp.class)
                .web(WebApplicationType.SERVLET)
                .run(args);
    }

    @Override
    public void run(String... args) {
        DeviceCreateEvent deviceCreateEvent=new DeviceCreateEvent();
        deviceCreateEvent.setDevice("C20110");
        deviceCreateEvent.setTs(System.currentTimeMillis());
        deviceCreateEvent.setCreateBy("666666666666666666666");
        eventPublisher.sendEvent(deviceCreateEvent);
    }
}

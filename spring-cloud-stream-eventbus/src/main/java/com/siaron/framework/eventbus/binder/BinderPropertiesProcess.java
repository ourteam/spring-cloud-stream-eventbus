package com.siaron.framework.eventbus.binder;


import com.siaron.framework.eventbus.model.FunctionInfo;

import java.util.Map;

/**
 * @author xielongwang
 * @create 2021/7/21 9:44 上午
 * @email siaron.wang@gmail.com
 * @description binder 属性处理
 */
public interface BinderPropertiesProcess {

    /**
     * 处理Binder
     *
     * @param key   binding key
     * @param value FunctionInfo
     * @return 具体Binder的 function 的Properties
     */
    Map<String, Object> processProperties(String key, FunctionInfo value);
}

package com.siaron.framework.eventbus.annotation;

import java.lang.annotation.*;

/**
 * @author xielongwang
 * @create 2021/7/20 11:30 上午
 * @email siaron.wang@gmail.com
 * @description 事件注解标注某个类为事件类.
 */
@Target({ElementType.TYPE})
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface Event {

    /**
     * kafka 中为topic / rabbitmq为 routing_key
     *
     * @return eventKey
     */
    String eventKey();

    /**
     * 发送那个那个binder
     *
     * @return binder
     */
    String[] binder();
}

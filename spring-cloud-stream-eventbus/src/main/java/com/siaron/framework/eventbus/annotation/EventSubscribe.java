package com.siaron.framework.eventbus.annotation;

import java.lang.annotation.*;

/**
 * @author xielongwang
 * @create 2021/7/19 2:30 下午
 * @email siaron.wang@gmail.com
 * @description 事件订阅注解.
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface EventSubscribe {

    /**
     * 随机消费主的前缀
     *
     * @return anonymousGroup
     */
    String anonymousGroup() default "";

    /**
     * 固定组名称
     *
     * @return group
     */
    String group() default "group";

    /**
     * 消费者
     *
     * @return consumer
     */
    int consumer() default 1;

    /**
     * 绑定那个binder
     *
     * @return binder
     */
    String binder() default "rabbit";
}

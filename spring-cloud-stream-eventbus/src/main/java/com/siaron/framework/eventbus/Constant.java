package com.siaron.framework.eventbus;

/**
 * @author xielongwang
 * @create 2021/7/22 9:16 上午
 * @email siaron.wang@gmail.com
 * @description 常量
 */
public interface Constant {

    String OUT_SUFFIX = "-out-0";

    String IN_SUFFIX = "-in-0";

    String UNDERLINE = "-";

    String UNDERSCORE = "_";

    String EVENTBUS="eventbus";

}

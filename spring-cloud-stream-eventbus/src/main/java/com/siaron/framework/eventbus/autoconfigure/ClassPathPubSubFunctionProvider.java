package com.siaron.framework.eventbus.autoconfigure;

import com.siaron.framework.eventbus.annotation.Event;
import com.siaron.framework.eventbus.annotation.EventPublish;
import com.siaron.framework.eventbus.annotation.EventSubscribe;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author xielongwang
 * @create 2021/7/20 12:59 下午
 * @email siaron.wang@gmail.com
 * @description pub sub function 扫描
 */
public class ClassPathPubSubFunctionProvider extends ClassPathBeanDefinitionScanner {

    public ClassPathPubSubFunctionProvider(BeanDefinitionRegistry registry, boolean useDefaultFilters) {

        super(registry, useDefaultFilters);
    }

    public void registerFilter() {
        addIncludeFilter(new AnnotationTypeFilter(Configuration.class));
        addIncludeFilter(new AnnotationTypeFilter(EventPublish.class));
        addIncludeFilter(new AnnotationTypeFilter(EventSubscribe.class));
        addIncludeFilter(new AnnotationTypeFilter(Event.class));
    }


    @Override
    protected Set<BeanDefinitionHolder> doScan(String... basePackages) {
        Set<BeanDefinitionHolder> beanDefinitions = new LinkedHashSet<>();
        for (String basePackage : basePackages) {
            Set<BeanDefinition> candidates = findCandidateComponents(basePackage);
            for (BeanDefinition candidate : candidates) {
                BeanDefinitionHolder definitionHolder = new BeanDefinitionHolder(candidate, candidate.getBeanClassName());
                beanDefinitions.add(definitionHolder);
            }
        }
        return beanDefinitions;
    }
}

package com.siaron.framework.eventbus.autoconfigure;

import com.siaron.framework.eventbus.annotation.ConditionalOnEventBusEnabled;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * @author xielongwang
 * @create 2021/7/14 7:54 下午
 * @email siaron.wang@gmail.com
 * @description 优先于 BindingServiceConfiguration 加载.用于注册function属性.
 */
@Configuration
@ConditionalOnEventBusEnabled
@EnableConfigurationProperties(EventBusProperties.class)
public class EventBusPropertiesAutoConfiguration {

    private final ConfigurableEnvironment configurableEnvironment;

    public EventBusPropertiesAutoConfiguration(ConfigurableEnvironment configurableEnvironment) {
        this.configurableEnvironment = configurableEnvironment;
    }

    @Bean
    public BindingServicePropertiesProcessor bindingServicePropertiesProcessor() {
        return new BindingServicePropertiesProcessor(configurableEnvironment);
    }
}

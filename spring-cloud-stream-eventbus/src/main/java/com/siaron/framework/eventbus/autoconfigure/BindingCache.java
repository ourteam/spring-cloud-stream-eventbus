package com.siaron.framework.eventbus.autoconfigure;


import com.siaron.framework.eventbus.model.FunctionInfo;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xielongwang
 * @create 2021/7/21 9:19 上午
 * @email siaron.wang@gmail.com
 * @description binding cache
 */
public class BindingCache {

    private static final Map<String, FunctionInfo> FUN = new HashMap<>();

    public static Map<String, FunctionInfo> getFun() {

        return FUN;
    }
}

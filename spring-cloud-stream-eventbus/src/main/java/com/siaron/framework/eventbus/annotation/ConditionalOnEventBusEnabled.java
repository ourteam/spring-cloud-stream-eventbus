package com.siaron.framework.eventbus.annotation;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author xielongwang
 * @create 2021/7/20 11:30 上午
 * @email siaron.wang@gmail.com
 * @description 判断是否开启EventBus
 */
@ConditionalOnProperty(value = ConditionalOnEventBusEnabled.EVENT_BUS_ENABLED, matchIfMissing = true)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface ConditionalOnEventBusEnabled {

    String EVENT_BUS_ENABLED = "spring.cloud.stream.eventbus.enable";
}

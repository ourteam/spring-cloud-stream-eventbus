package com.siaron.framework.eventbus.utils;

/**
 * @author xielongwang
 * @create 2020/6/7 10:39 上午
 * @email siaron.wang@gmail.com
 * @description spring bean name
 */
public class BeanNameUtil {

    /**
     * Utility method to take a string and convert it to normal Java variable
     * name capitalization.  This normally means converting the first
     * character from upper case to lower case, but in the (unusual) special
     * case when there is more than one character and both the first and
     * second characters are upper case, we leave it alone.
     * <p>
     * Thus "FooBah" becomes "fooBah" and "X" becomes "x", but "URL" stays
     * as "URL".
     *
     * @param name The string to be decapitalized.
     * @return The decapitalized version of the string.
     */
    public static String decapitalize(String name) {
        if (name == null || name.length() == 0) {
            return name;
        }
        int offset1 = Character.offsetByCodePoints(name, 0, 1);
        // Should be name.offsetByCodePoints but 6242664 makes this fail
        if (offset1 < name.length() &&
                Character.isUpperCase(name.codePointAt(offset1))) {
            return name;
        }
        return name.substring(0, offset1).toLowerCase() +
                name.substring(offset1);
    }
}

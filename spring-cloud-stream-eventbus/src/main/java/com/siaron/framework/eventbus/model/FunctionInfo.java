package com.siaron.framework.eventbus.model;

import com.siaron.framework.eventbus.binder.BinderRelation;
import org.springframework.cloud.stream.config.BindingProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xielongwang
 * @create 2021/7/20 7:19 下午
 * @email siaron.wang@gmail.com
 * @description
 */
public class FunctionInfo {
    /**
     * in or out
     */
    private FunType type;

    /**
     * not (out /in) suffix == functionName
     */
    private String funName;

    /**
     * eventName
     */
    private String event;

    /**
     * eventKey
     */
    private String eventKey;

    /**
     * binders
     */
    private String binder;

    /**
     * consumer
     */
    private int consumer;

    /**
     * anonymousGroup
     */
    private String anonymousGroup;

    /**
     * bindersType
     */
    private BinderRelation bindersType;

    /**
     * BindingProperties
     */
    private BindingProperties bindingProperties;

    /**
     * binder
     */
    private Map<String, Object> binderBindings = new HashMap<>();


    public FunType getType() {
        return type;
    }

    public void setType(FunType type) {
        this.type = type;
    }

    public String getFunName() {
        return funName;
    }

    public void setFunName(String funName) {
        this.funName = funName;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getEventKey() {
        return eventKey;
    }

    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }

    public String getBinder() {
        return binder;
    }

    public void setBinder(String binder) {
        this.binder = binder;
    }

    public int getConsumer() {
        return consumer;
    }

    public void setConsumer(int consumer) {
        this.consumer = consumer;
    }

    public String getAnonymousGroup() {
        return anonymousGroup;
    }

    public void setAnonymousGroup(String anonymousGroup) {
        this.anonymousGroup = anonymousGroup;
    }

    public BinderRelation getBindersType() {
        return bindersType;
    }

    public void setBindersType(BinderRelation bindersType) {
        this.bindersType = bindersType;
    }

    public BindingProperties getBindingProperties() {
        return bindingProperties;
    }

    public void setBindingProperties(BindingProperties bindingProperties) {
        this.bindingProperties = bindingProperties;
    }

    public Map<String, Object> getBinderBindings() {
        return binderBindings;
    }

    public void setBinderBindings(Map<String, Object> binderBindings) {
        this.binderBindings = binderBindings;
    }
}

package com.siaron.framework.eventbus.binder;

import com.siaron.framework.eventbus.model.FunctionInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.stream.config.BindingProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xielongwang
 * @create 2021/7/21 6:09 下午
 * @email siaron.wang@gmail.com
 * @description rocketmq 处理
 */
public class RocketmqBinderPropertiesProcess implements BinderPropertiesProcess {

    @Override
    public Map<String, Object> processProperties(String binding, FunctionInfo info) {
        BindingProperties bindingProperties = info.getBindingProperties();
        bindingProperties.setDestination(info.getEventKey());
        //将topic 和eventKey 中的','转成中划线
        bindingProperties.setGroup(replaceToUnderline(bindingProperties.getGroup()));
        info.setEventKey(replaceToUnderline(info.getEvent()));
        return new HashMap<>(1);
    }

    private String replaceToUnderline(String str) {
        if (StringUtils.isBlank(str)) {
            return str;
        }
        return str.replace(".", "-");
    }
}

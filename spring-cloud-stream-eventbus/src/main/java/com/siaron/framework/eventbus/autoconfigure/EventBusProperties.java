package com.siaron.framework.eventbus.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;


/**
 * @author xielongwang
 * @create 2021/7/19 4:17 下午
 * @email siaron.wang@gmail.com
 * EventBusProperties 配置
 */
@ConfigurationProperties(EventBusProperties.PREFIX)
public class EventBusProperties {

    public static final String PREFIX = "spring.cloud.stream.eventbus";

    public boolean enable;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}

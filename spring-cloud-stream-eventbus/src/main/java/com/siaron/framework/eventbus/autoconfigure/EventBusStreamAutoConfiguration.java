package com.siaron.framework.eventbus.autoconfigure;

import com.siaron.framework.eventbus.annotation.ConditionalOnEventBusEnabled;
import com.siaron.framework.eventbus.publisher.EventPublisher;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.cloud.stream.function.FunctionConfiguration;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author xielongwang
 * @create 2021/7/19 4:06 下午
 * @email siaron.wang@gmail.com
 * @description 事件发布者configuration
 */
@Configuration
@ConditionalOnEventBusEnabled
@AutoConfigureAfter(FunctionConfiguration.class)
public class EventBusStreamAutoConfiguration {

    @Bean
    public EventPublisher eventPublisher(StreamBridge streamBridge) {

        return new EventPublisher(streamBridge);
    }
}

package com.siaron.framework.eventbus.annotation;

import com.siaron.framework.eventbus.publisher.EventPublisher;

import java.lang.annotation.*;

/**
 * @author xielongwang
 * @create 2021/7/19 4:04 下午
 * @email siaron.wang@gmail.com
 * @description 事件发布者或 {@link EventPublisher}
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface EventPublish {

    String binder() default "rabbit";
}

package com.siaron.framework.eventbus.binder;

import com.siaron.framework.eventbus.model.FunType;
import com.siaron.framework.eventbus.model.FunctionInfo;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xielongwang
 * @create 2021/7/21 10:04 上午
 * @email siaron.wang@gmail.com
 * @description
 */
public class RabbitmqBinderPropertiesProcess implements BinderPropertiesProcess {

    private static final String BINDER_PREFIX = "spring.cloud.stream.rabbit.bindings.";

    @Override
    public Map<String, Object> processProperties(String key, FunctionInfo info) {
        Map<String, Object> propertiesMap = new HashMap<>(11);
        if (info.getType().equals(FunType.IN)) {
            propertiesMap.put(BINDER_PREFIX + key + ".consumer.exchange-type", "direct");
            propertiesMap.put(BINDER_PREFIX + key + ".consumer.auto-bind-dlq", true);
            propertiesMap.put(BINDER_PREFIX + key + ".consumer.queue-name-group-only", true);
            propertiesMap.put(BINDER_PREFIX + key + ".consumer.binding-routing-key", info.getEventKey());
            propertiesMap.put(BINDER_PREFIX + key + ".consumer.binding-routing-key-delimiter", ",");
            if (StringUtils.isNotBlank(info.getAnonymousGroup())) {
                propertiesMap.put(BINDER_PREFIX + key + ".consumer.anonymous-group-prefix", info.getAnonymousGroup() + ".");
            }
        }

        if (info.getType().equals(FunType.OUT)) {
            propertiesMap.put(BINDER_PREFIX + key + ".producer.exchange-type", "direct");
            propertiesMap.put(BINDER_PREFIX + key + ".producer.routing-key-expression", "'" + info.getEventKey() + "'");
        }
        return propertiesMap;
    }
}

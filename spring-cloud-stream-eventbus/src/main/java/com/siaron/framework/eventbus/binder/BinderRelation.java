package com.siaron.framework.eventbus.binder;

import java.util.Arrays;
import java.util.HashMap;

/**
 * @author xielongwang
 * @create 2021/7/21 10:21 上午
 * @email siaron.wang@gmail.com
 * @description
 */
public enum BinderRelation {

    /**
     * rabbitmq
     */
    rabbit("rabbit", new RabbitmqBinderPropertiesProcess()),

    /**
     * kafka
     */
    kafka("kafka", new KafkaBinderPropertiesProcess()),

    /**
     * kafka
     */
    rocketmq("rocketmq", new RocketmqBinderPropertiesProcess()),

    /**
     * default
     */
    unknown("unknown", (key, value) -> {
        return new HashMap<>();
    });

    private String binder;

    private BinderPropertiesProcess binderPropertiesProcess;

    BinderRelation(String binder, BinderPropertiesProcess binderPropertiesProcess) {
        this.binder = binder;
        this.binderPropertiesProcess = binderPropertiesProcess;
    }

    public static BinderRelation binderOf(String binder) {

        return Arrays.stream(BinderRelation.values())
                .filter(alarmGrade -> alarmGrade.getBinder().equals(binder))
                .findFirst().orElse(BinderRelation.unknown);
    }

    public String getBinder() {
        return binder;
    }

    public void setBinder(String binder) {
        this.binder = binder;
    }

    public BinderPropertiesProcess getBinderPropertiesProcess() {
        return binderPropertiesProcess;
    }

    public void setBinderPropertiesProcess(BinderPropertiesProcess binderPropertiesProcess) {
        this.binderPropertiesProcess = binderPropertiesProcess;
    }
}


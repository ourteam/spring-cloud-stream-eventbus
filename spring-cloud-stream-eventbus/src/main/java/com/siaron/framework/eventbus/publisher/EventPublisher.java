package com.siaron.framework.eventbus.publisher;

import com.siaron.framework.eventbus.Constant;
import com.siaron.framework.eventbus.annotation.Event;
import com.siaron.framework.eventbus.model.AbstractEvent;
import org.springframework.cloud.stream.function.StreamBridge;

/**
 * @author xielongwang
 * @create 2021/7/14 8:20 下午
 * @email siaron.wang@gmail.com
 * @description event 发布
 */
public class EventPublisher {

    public StreamBridge streamBridge;

    public EventPublisher(StreamBridge streamBridge) {
        this.streamBridge = streamBridge;
    }

    public void sendEvent(AbstractEvent event) {
        Event annotation = event.getClass().getAnnotation(Event.class);
        if (annotation != null) {
            String name = event.getClass().getSimpleName();
            String[] binder = annotation.binder();
            for (String der : binder) {
                streamBridge.send(name + Constant.UNDERSCORE + der + Constant.OUT_SUFFIX, event);
            }
        }
    }

    public StreamBridge getStreamBridge() {
        return streamBridge;
    }

    public void setStreamBridge(StreamBridge streamBridge) {
        this.streamBridge = streamBridge;
    }
}

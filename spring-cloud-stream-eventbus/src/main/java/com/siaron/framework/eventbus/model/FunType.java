package com.siaron.framework.eventbus.model;

/**
 * @author xielongwang
 * @create 2021/7/21 1:14 下午
 * @email siaron.wang@gmail.com
 * @description
 */
public enum FunType {

    /**
     * 消费
     */
    IN,

    /**
     * 生产
     */
    OUT,
}

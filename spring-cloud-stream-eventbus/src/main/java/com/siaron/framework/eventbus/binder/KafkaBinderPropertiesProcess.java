package com.siaron.framework.eventbus.binder;

import com.siaron.framework.eventbus.model.FunctionInfo;
import org.springframework.cloud.stream.config.BindingProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xielongwang
 * @create 2021/7/21 4:09 下午
 * @email siaron.wang@gmail.com
 * @description
 */
public class KafkaBinderPropertiesProcess implements BinderPropertiesProcess{

    private static final String BINDER_PREFIX = "spring.cloud.stream.kafka.bindings.";

    @Override
    public Map<String, Object> processProperties(String key, FunctionInfo value) {
        //替换下 Destination
        BindingProperties bindingProperties = value.getBindingProperties();
        bindingProperties.setDestination(value.getEventKey());
        return new HashMap<>(1);
    }
}

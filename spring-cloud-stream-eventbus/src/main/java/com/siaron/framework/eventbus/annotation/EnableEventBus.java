package com.siaron.framework.eventbus.annotation;

import com.siaron.framework.eventbus.autoconfigure.EventPubSubSelector;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author xielongwang
 * @create 2021/7/20 10:35 上午
 * @email siaron.wang@gmail.com
 * @description eventbus function scan
 */
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Configuration
@Import({EventPubSubSelector.class})
public @interface EnableEventBus {

    /**
     * Event / EventPublish / EventSubscribe 所在的包
     *
     * @return the array of 'basePackages'.
     */
    String[] basePackages() default {};

}
